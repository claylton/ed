/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atv.netflix;

import atv.netflix.pessoas.Pessoas;
import atv.netflix.pessoas.Ator;
import atv.netflix.pessoas.Diretor;
import atv.netflix.titulos.Filmes;
import atv.netflix.titulos.Series;
import atv.netflix.titulos.Titulos;
import java.util.Scanner;
import lde.ListaDuplamenteEncadeada;

/**
 *
 * @author ClayltonS
 */
public class Main {

    public static void main(String[] args) {
        ListaDuplamenteEncadeada<Series> listaSeries = new ListaDuplamenteEncadeada();
        ListaDuplamenteEncadeada<Filmes> listaFilmes = new ListaDuplamenteEncadeada();
        ListaDuplamenteEncadeada<Ator> listaAtor = new ListaDuplamenteEncadeada();
        ListaDuplamenteEncadeada<Diretor> listaDiretor = new ListaDuplamenteEncadeada();

        Scanner ler = new Scanner(System.in);
        int opc;

        do {
            System.out.println("------------------------MENU-----------------------");
            System.out.println("1 - Incluir Titulo");
            System.out.println("2 - Incluir Pessoa");
            System.out.println("3 - Alterar Titulo");
            System.out.println("4 - Alterar Pessoa");
            System.out.println("5 - Excluir Titulo/Pessoa");
            System.out.println("6 - Consultar");
            System.out.println("7 - Avaliar Título");
            System.out.println("0 - Sair");
            System.out.println("----------------------------------------------------");
            System.out.print("Opção: ");
            opc = Integer.parseInt(ler.next());
            System.out.println("----------------------------------------------------");

            switch (opc) {
                case 1:
                    int opc1;
                    System.out.println("------------------------MENU-----------------------");
                    System.out.println("1 - Filme");
                    System.out.println("2 - Série");
                    System.out.println("----------------------------------------------------");
                    System.out.print("Opção: ");
                    opc1 = Integer.parseInt(ler.next());
                    System.out.println("----------------------------------------------------");

                    switch (opc1) {
                        case 1:
                            System.out.print("Nome: ");
                            String nome = ler.next();

                            String tipo = "Filme";

                            System.out.print("Categoria: ");
                            String categoria = ler.next();

                            System.out.print("Subcategoria: ");
                            String subcate = ler.next();

                            System.out.print("Descrição: ");
                            String desc = ler.next();

                            int ava = 0;

                            Filmes f = new Filmes(nome, tipo, categoria, subcate, desc, ava);
                            listaFilmes.adicionar(f);
                            break;

                        case 2:
                            System.out.print("Nome: ");
                            String nome1 = ler.next();

                            String tipo1 = "Série";

                            System.out.print("Categoria: ");
                            String categoria1 = ler.next();

                            System.out.print("Subcategoria: ");
                            String subcate1 = ler.next();

                            System.out.print("Descrição: ");
                            String desc1 = ler.next();

                            int ava1 = 0;

                            Series s = new Series(nome1, tipo1, categoria1, subcate1, desc1, ava1);
                            listaSeries.adicionar(s);
                            break;
                    }

                    break;

                case 2:
                    int opc2;
                    System.out.println("------------------------MENU-----------------------");
                    System.out.println("1 - Ator");
                    System.out.println("2 - Diretor");
                    System.out.println("----------------------------------------------------");
                    System.out.print("Opção: ");
                    opc2 = Integer.parseInt(ler.next());
                    System.out.println("----------------------------------------------------");

                    switch (opc2) {
                        case 1:
                            System.out.print("Nome: ");
                            String nome = ler.next();

                            String func = "Ator";

                            Ator a = new Ator(nome, func);
                            listaAtor.adicionar(a);
                            break;
                        case 2:
                            System.out.print("Nome: ");
                            String nome1 = ler.next();

                            String func1 = "Diretor";

                            Diretor d = new Diretor(nome1, func1);
                            listaDiretor.adicionar(d);
                            break;
                    }

                    break;

                case 3:
                    int opc3;
                    System.out.println("------------------------MENU-----------------------");
                    System.out.println("1 - Filme");
                    System.out.println("2 - Série");
                    System.out.println("----------------------------------------------------");
                    System.out.print("Opção: ");
                    opc3 = Integer.parseInt(ler.next());
                    System.out.println("----------------------------------------------------");
                    switch (opc3) {
                        case 1:
                            System.out.print("Nome: ");
                            String nome = ler.next();
                            for (Titulos t : listaFilmes) {
                                if (t.getNome().equals(nome)) {

                                    System.out.print("Nome: ");
                                    String nomeA = ler.next();
                                    t.setNome(nomeA);

                                    System.out.println("Tipo: ");
                                    String tipo = ler.next();
                                    t.setTipo(tipo);

                                    System.out.print("Categoria: ");
                                    String categoria = ler.next();
                                    t.setCate(categoria);

                                    System.out.print("Subcategoria: ");
                                    String subcate = ler.next();
                                    t.setSubCate(subcate);

                                    System.out.print("Descrição: ");
                                    String desc = ler.next();
                                    t.setDescri(desc);
                                    break;
                                }
                            }
                            break;

                        case 2:
                            System.out.print("Nome: ");
                            String nome1 = ler.next();
                            for (Titulos s : listaSeries) {
                                if (s.getNome().equals(nome1)) {

                                    System.out.print("Nome: ");
                                    String nomeA = ler.next();
                                    s.setNome(nomeA);

                                    System.out.println("Tipo: ");
                                    String tipo = ler.next();
                                    s.setTipo(tipo);

                                    System.out.print("Categoria: ");
                                    String categoria = ler.next();
                                    s.setCate(categoria);

                                    System.out.print("Subcategoria: ");
                                    String subcate = ler.next();
                                    s.setSubCate(subcate);

                                    System.out.print("Descrição: ");
                                    String desc = ler.next();
                                    s.setDescri(desc);
                                    break;
                                }
                            }
                            break;
                    }

                    break;

                case 4:
                    int opc4;
                    System.out.println("------------------------MENU-----------------------");
                    System.out.println("1 - Ator");
                    System.out.println("2 - Diretor");
                    System.out.println("----------------------------------------------------");
                    System.out.print("Opção: ");
                    opc4 = Integer.parseInt(ler.next());
                    System.out.println("----------------------------------------------------");
                    switch (opc4) {
                        case 1:
                            System.out.print("Nome: ");
                            String nome = ler.next();
                            for (Pessoas a : listaAtor) {
                                if (a.getNome().equals(nome)) {

                                    System.out.print("Nome: ");
                                    String nomeA = ler.next();
                                    a.setNome(nomeA);

                                    System.out.println("Tipo: ");
                                    String tipo = ler.next();
                                    a.setFunc(tipo);
                                    break;
                                }
                            }
                            break;

                        case 2:
                            System.out.print("Nome: ");
                            String nome1 = ler.next();
                            for (Pessoas a : listaAtor) {
                                if (a.getNome().equals(nome1)) {

                                    System.out.print("Nome: ");
                                    String nomeA = ler.next();
                                    a.setNome(nomeA);

                                    System.out.println("Tipo: ");
                                    String tipo = ler.next();
                                    a.setFunc(tipo);
                                    break;
                                }
                            }
                            break;
                    }

                    break;
                case 5:
                    System.out.print("Nome(Titulo/Pessoa): ");
                    String nome = ler.next();

                    for (Titulos t : listaFilmes) {
                        if (t.getNome().equals(nome)) {
                            listaFilmes.removeItem((Filmes) t);
                            break;
                        }
                    }
                    for (Titulos s : listaSeries) {
                        if (s.getNome().equals(nome)) {
                            listaSeries.removeItem((Series) s);
                            break;
                        }
                    }
                    for (Pessoas a : listaAtor) {
                        if (a.getNome().equals(nome)) {
                            listaAtor.removeItem((Ator) a);
                            break;
                        }
                    }
                    for (Pessoas d : listaDiretor) {
                        if (d.getNome().equals(nome)) {
                            listaDiretor.removeItem((Diretor) d);
                            break;
                        }
                    }
                    break;

                case 6:
                    System.out.print("Nome(Titulo/Pessoa/Categoria/Subcategoria): ");
                    String nomeT = ler.next();

                    for (Titulos t : listaFilmes) {
                        if (t.getNome().equals(nomeT) || t.getCate().equals(nomeT) || t.getSubCate().equals(nomeT)) {
                            System.out.println("Nome: " + t.getNome());
                            System.out.println("Tipo: " + t.getTipo());
                            System.out.println("Categoria: " + t.getCate());
                            System.out.println("Subcategoria: " + t.getSubCate());
                            System.out.println("Descrição: " + t.getDescri());
                            System.out.println("Avalição: " + t.getAva());
                            break;
                        }
                    }
                    for (Titulos s : listaSeries) {
                        if (s.getNome().equals(nomeT) || s.getCate().equals(nomeT) || s.getSubCate().equals(nomeT)) {
                            System.out.println("Nome: " + s.getNome());
                            System.out.println("Tipo: " + s.getTipo());
                            System.out.println("Categoria: " + s.getCate());
                            System.out.println("Subcategoria: " + s.getSubCate());
                            System.out.println("Descrição: " + s.getDescri());
                            System.out.println("Avalição: " + s.getAva());
                            break;
                        }
                    }
                    for (Pessoas a : listaAtor) {
                        if (a.getNome().equals(nomeT)) {
                            System.out.println("Nome: " + a.getNome());
                            System.out.println("Função: " + a.getFunc());
                            break;
                        }
                    }
                    for (Pessoas d : listaDiretor) {
                        if (d.getNome().equals(nomeT)) {
                            System.out.println("Nome: " + d.getNome());
                            System.out.println("Função: " + d.getFunc());
                            break;
                        }
                    }
                    break;

                case 7:
                    int opc5;
                    System.out.println("------------------------MENU-----------------------");
                    System.out.println("1 - Filme");
                    System.out.println("2 - Série");
                    System.out.println("----------------------------------------------------");
                    System.out.print("Opção: ");
                    opc3 = Integer.parseInt(ler.next());
                    System.out.println("----------------------------------------------------");
                    switch (opc3) {
                        case 1:
                            System.out.println("Digite o nome do Filme: ");
                            nome = ler.next();

                            for (Titulos t : listaFilmes) {
                                if (t.getNome().equals(nome)) {

                                    System.out.println("digite a avaliação: ");
                                    double nota = ler.nextDouble();

                                    for (Filmes f : listaFilmes) {
                                        f.setAvaCont(f.getAvaCont() + 1);
                                        f.setSomatorio(f.getSomatorio() + nota);

                                        t.setAva(f.getSomatorio() / f.getAvaCont());
                                    }

                                }
                            }
                            break;

                        case 2:
                            System.out.println("Digite o nome da Série: ");
                            nome = ler.next();

                            for (Titulos t : listaSeries) {
                                if (t.getNome().equals(nome)) {

                                    System.out.println("digite a avaliação: ");
                                    double nota = ler.nextDouble();

                                    for (Series s : listaSeries) {
                                        s.setAvaCont(s.getAvaCont() + 1);
                                        s.setSomatorio(s.getSomatorio() + nota);

                                        t.setAva(s.getSomatorio() / s.getAvaCont());
                                    }

                                }
                            }
                            break;
                    }
                    break;
            }
        } while (opc != 0);

    }
}
