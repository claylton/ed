package atv.netflix.pessoas;

import lde.ListaDuplamenteEncadeada;

public class Pessoas {

    private String nome;
    private String func;
    ListaDuplamenteEncadeada<Ator> ator = new ListaDuplamenteEncadeada();
    ListaDuplamenteEncadeada<Diretor> diretor = new ListaDuplamenteEncadeada();

    public Pessoas(String nome, String func) {
        this.nome = nome;
        this.func = func;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }

    public ListaDuplamenteEncadeada<Ator> getAtor() {
        return ator;
    }

    public void setAtor(ListaDuplamenteEncadeada<Ator> ator) {
        this.ator = ator;
    }

    public ListaDuplamenteEncadeada<Diretor> getDiretor() {
        return diretor;
    }

    public void setDiretor(ListaDuplamenteEncadeada<Diretor> diretor) {
        this.diretor = diretor;
    }
    
}
