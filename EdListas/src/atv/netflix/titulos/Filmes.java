/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atv.netflix.titulos;

import atv.netflix.pessoas.Pessoas;
import atv.netflix.titulos.Titulos;
import lde.ListaDuplamenteEncadeada;

/**
 *
 * @author ClayltonS
 */
public class Filmes extends Titulos {

    private int avaCont, nota;
    private double somatorio;

    public Filmes(String nome, String tipo, String cate, String subCate, String descri, double ava) {
        super(nome, tipo, cate, subCate, descri, ava);
    }

    public int getAvaCont() {
        return avaCont;
    }

    public void setAvaCont(int avaCont) {
        this.avaCont = avaCont;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public double getSomatorio() {
        return somatorio;
    }

    public void setSomatorio(double somatorio) {
        this.somatorio = somatorio;
    }

}
