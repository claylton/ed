/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atv.netflix.titulos;

import atv.netflix.pessoas.Pessoas;
import lde.ListaDuplamenteEncadeada;

/**
 *
 * @author ClayltonS
 */
public class Titulos {

    private String nome, tipo, cate, subCate, descri;
    private double ava;
    
    ListaDuplamenteEncadeada<Filmes> filmes = new ListaDuplamenteEncadeada<>();
    ListaDuplamenteEncadeada<Series> series = new ListaDuplamenteEncadeada<>();

    public Titulos(String nome, String tipo, String cate, String subCate, String descri, double ava) {
        this.nome = nome;
        this.tipo = tipo;
        this.cate = cate;
        this.subCate = subCate;
        this.descri = descri;
        this.ava = ava;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCate() {
        return cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public String getSubCate() {
        return subCate;
    }

    public void setSubCate(String subCate) {
        this.subCate = subCate;
    }

    public String getDescri() {
        return descri;
    }

    public void setDescri(String descri) {
        this.descri = descri;
    }

    public double getAva() {
        return ava;
    }

    public void setAva(double ava) {
        this.ava = ava;
    }

    public ListaDuplamenteEncadeada<Filmes> getFilmes() {
        return filmes;
    }

    public void setFilmes(ListaDuplamenteEncadeada<Filmes> filmes) {
        this.filmes = filmes;
    }

    public ListaDuplamenteEncadeada<Series> getSeries() {
        return series;
    }

    public void setSeries(ListaDuplamenteEncadeada<Series> series) {
        this.series = series;
    }

}
