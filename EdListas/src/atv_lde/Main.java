package atv_lde;

import java.util.Scanner;
import lse.ListaEncadeada;

public class Main {

    public static void main(String[] args) {
        ListaEncadeada<Pessoa> listaPessoa = new ListaEncadeada<>();
        Scanner ler = new Scanner(System.in);
        int opc;

        do {
            System.out.println("------------------------MENU-----------------------");
            System.out.println("1 - Incluir Pessoa");
            System.out.println("2 - Excluir Pessoa");
            System.out.println("3 - Consultar Pessoa");
            System.out.println("4 - Listar Pessoas");
            System.out.println("5 - Incluir Bem");
            System.out.println("6 - Excluir Bem");
            System.out.println("0 - Sair");
            System.out.println("----------------------------------------------------");
            System.out.print("Opção: ");
            opc = Integer.parseInt(ler.next());
            System.out.println("----------------------------------------------------");

            switch (opc) {
                case 1:
                    boolean achou = false;

                    System.out.print("Código: ");
                    int cod = Integer.parseInt(ler.next());

                    for (Pessoa p1 : listaPessoa) {
                        if (cod == p1.getCodigo()) {
                            achou = true;
                            System.out.println("Esse código já existe no sistema");
                            break;
                        }
                    }
                    if (achou) {
                        break;
                    }
                    
                    System.out.print("Nome: ");
                    String nome = ler.next();

                    Pessoa p = new Pessoa(cod, nome);
                    listaPessoa.adicionar(p);
                    break;

                case 2:
                    for (Pessoa p1 : listaPessoa) {
                        System.out.println(p1.getCodigo() + "\t" + p1.getNome());
                    }
                    System.out.println("Digite o Código");
                    int cod1 = Integer.parseInt(ler.next());

                    for (Pessoa p1 : listaPessoa) {
                        if (cod1 == p1.getCodigo()) {
                            listaPessoa.removeItem(p1);
                            break;
                        }
                    }
                    break;

                case 3:
                    for (Pessoa p1 : listaPessoa) {
                        System.out.println(p1.getCodigo() + "\t" + p1.getNome());
                    }
                    System.out.println("Digite o Código");
                    int cod2 = Integer.parseInt(ler.next());

                    for (Pessoa p1 : listaPessoa) {
                        if (p1.getCodigo() == cod2) {
                            System.out.println("COD PESSOA: " + p1.getCodigo() + "\t"
                                    + "  NOME: " + p1.getNome());
                            System.out.println("RELAÇÃO DE BENS");
                            System.out.println("COD BEM\t NOME DO BEM \tVALOR");

                            for (Bem b : p1.getBens()) {
                                System.out.println(b.getCodigo() + "\t" + b.getNome() + "\t "
                                        + b.getValor());
                            }
                        }
                    }
                    break;

                case 4:
                    for (Pessoa pessoa : listaPessoa) {
                        System.out.println("COD PESSOA \t NOME PESSOA \t VALOR TOTAL DE BENS");
                        System.out.println(pessoa.getCodigo() + "\t\t  " + pessoa.getNome() + "\t" + "\t" + pessoa.getTotalBens());

                    }
                    break;

                case 5:
                    System.out.println("Digite o código do Bem: ");
                    int codigo = Integer.parseInt(ler.next());

                    System.out.println("Digite o nome do Bem: ");
                    String nBem = ler.next();

                    System.out.println("Digite o Valor do Bem: ");
                    double valor = Double.parseDouble(ler.next());

                    Bem bem = new Bem(codigo, nBem, valor);

                    System.out.println("----------------------------------------------------");
                    System.out.println("COD\tNOME");
                    for (Pessoa p1 : listaPessoa) {
                        System.out.println(p1.getCodigo()
                                + "\t" + p1.getNome());
                    }
                    System.out.println("----------------------------------------------------");

                    System.out.print("Inserir código da pessoa para o bem ser adicionado: ");
                    int cod3 = ler.nextInt();
                    System.out.println("----------------------------------------------------");

                    for (Pessoa p1 : listaPessoa) {
                        if (p1.getCodigo() != cod3)
                            continue;
                        boolean achou1 = false;
                        for (Bem b : p1.getBens()) {
                            if (codigo == b.getCodigo()) {
                                achou1 = true;
                                break;
                            }
                        }
                        if (achou1) {
                            System.out.println("Código de bem já incluso na lista desta pessoa !");
                        } else {
                            p1.getBens().adicionarNoFim(bem);
                            p1.setTotalBens(p1.getTotalBens() + valor);
                        }
                    }
                    break;
                case 6:
                    for (Pessoa p1 : listaPessoa) {
                        for (Bem b : p1.getBens()) {
                            System.out.println(b.getCodigo() + "\t" + b.getNome());
                        }
                    }

                    System.out.println("Digite o Código");
                    int cod4 = Integer.parseInt(ler.next());

                    for (Pessoa p1 : listaPessoa) {
                        for (Bem b : p1.getBens()) {
                            if (cod4 == b.getCodigo()) {
                                p1.getBens().removeItem(b);
                                p1.setTotalBens(p1.getTotalBens() - b.getValor());
                                break;
                            }
                        }
                    }
                    break;
            }
        } while (opc != 0);

    }
}
