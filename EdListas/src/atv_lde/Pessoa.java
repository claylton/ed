package atv_lde;

import java.util.Objects;
import lde.ListaDuplamenteEncadeada;

public class Pessoa {
    private int codigo;
    private String nome;
    ListaDuplamenteEncadeada<Bem>bens = new ListaDuplamenteEncadeada<>();
    private double totalBens;

    public double getTotalBens() {
        return totalBens;
    }

    public void setTotalBens(double totalBens) {
        this.totalBens = totalBens;
    }

    public Pessoa(int codigo, String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ListaDuplamenteEncadeada<Bem> getBens() {
        return bens;
    }

    public void setBens(ListaDuplamenteEncadeada<Bem> bens) {
        this.bens = bens;
    }

}