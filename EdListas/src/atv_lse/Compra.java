package atv_lse;

public class Compra {
    private int qtdCompra;
    private String dataCompra,nomeCompra;
    private float valorCompra;

    public Compra( String dataCompra,int qtdCompra, float valorCompra, String nomeCompra) {
        this.qtdCompra = qtdCompra;
        this.dataCompra = dataCompra;
        this.valorCompra = valorCompra;
        this.nomeCompra = nomeCompra;
    }

    public int getQtdCompra() {
        return qtdCompra;
    }

    public void setQtdCompra(int qtdCompra) {
        this.qtdCompra = qtdCompra;
    }

    public String getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(String dataCompra) {
        this.dataCompra = dataCompra;
    }

    public float getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(float valorCompra) {
        this.valorCompra = valorCompra;
    }

    public String getNomeCompra() {
        return nomeCompra;
    }

    public void setNomeCompra(String nomeCompra) {
        this.nomeCompra = nomeCompra;
    }
}