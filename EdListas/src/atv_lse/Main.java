package atv_lse;

import java.util.Scanner;
import lse.ListaEncadeada;

public class Main {

    public static void main(String[] args) {
        ListaEncadeada<Produto> listaProduto = new ListaEncadeada<>();
        Scanner ler = new Scanner(System.in);
        int opc;

        do {
            System.out.println("------------------------MENU-----------------------");
            System.out.println("1 - Cadastrar Produto");
            System.out.println("2 - Registrar Compra");
            System.out.println("3 - Registrar Venda");
            System.out.println("4 - Exibir informações de um produto");
            System.out.println("5 - Imprimir Compras");
            System.out.println("6 - Imprimir Vendas");
            System.out.println("0 - Sair");
            System.out.println("----------------------------------------------------");
            System.out.print("Opção: ");
            opc = Integer.parseInt(ler.nextLine());
            System.out.println("----------------------------------------------------");

            switch (opc) {
                case 1:
                    System.out.print("Código: ");
                    int cod = Integer.parseInt(ler.nextLine());
                    boolean achou = false;
                    for (Produto p1 : listaProduto) {
                        if (cod == p1.getCodProduto()) {
                            achou = true;
                            System.out.println("O código já existe no sistema");
                            break;
                        }
                    }
                    if (achou) {
                        break;
                    }
                    System.out.print("Nome: ");
                    String nome = ler.nextLine();

                    Produto p = new Produto(cod, nome);
                    listaProduto.adicionar(p);
                    break;

                case 2:
                    for (Produto p1 : listaProduto) {
                        System.out.println(p1.getCodProduto() + "\t" + p1.getnProduto());
                    }
                    System.out.println("Digite o Código do Produto");
                    int cod1 = Integer.parseInt(ler.nextLine());

                    for (Produto p1 : listaProduto) {
                        if (p1.getCodProduto() == cod1) {
                            System.out.print("Data da compra: ");
                            String data = ler.nextLine();

                            System.out.print("Quantidade: ");
                            int qtd1 = Integer.parseInt(ler.nextLine());
                            int qtdAtual = p1.getQtdEstoque() + qtd1;
                            p1.setQtdEstoque(qtdAtual);

                            System.out.print("Valor: ");
                            float valor = Float.parseFloat(ler.nextLine());

                            float uni = (valor / qtd1);
                            p1.setCustoUni(uni);

                            String nProd = p1.getnProduto();

                            Compra c = new Compra(data, qtd1, valor, nProd);
                            p1.getListaCompra().adicionar(c);

                        }
                    }
                    break;

                case 3:
                    for (Produto p1 : listaProduto) {
                        System.out.println(p1.getCodProduto() + "\t" + p1.getnProduto());
                    }
                    System.out.println("Digite o Código do Produto");
                    int cod2 = Integer.parseInt(ler.nextLine());
                    for (Produto p1 : listaProduto) {
                        if (p1.getCodProduto() == cod2) {
                            System.out.print("Data: ");
                            String data = ler.nextLine();

                            System.out.print("Quantidade: ");
                            int qtd1 = Integer.parseInt(ler.nextLine());
                            if (qtd1 > p1.getQtdEstoque()) {
                                System.out.println("ERRO: Sem quantidade suficiente no estoque");
                                break;
                            }
                            int qtdAtual = p1.getQtdEstoque() - qtd1;
                            p1.setQtdEstoque(qtdAtual);

                            float valor = p1.getCustoUni() * qtd1;
                            System.out.println("Valor: " + valor);

                            float uni = (valor / qtd1);
                            System.out.println("Custo Unitário: " + uni);

                            String nProd = p1.getnProduto();

                            Venda v = new Venda(data, qtd1, valor, uni, nProd);
                            p1.getListaVenda().adicionar(v);
                        }
                    }
                    break;

                case 4:
                    System.out.println("CÓDIGO\tNOME");
                    for (Produto p1 : listaProduto) {
                        System.out.println(p1.getCodProduto() + "\t" + p1.getnProduto());
                    }
                    System.out.println("Digite o Código do Produto");
                    int cod3 = Integer.parseInt(ler.nextLine());
                    System.out.println("----------------------PRODUTOS----------------------");
                    for (Produto p1 : listaProduto) {
                        if (p1.getCodProduto() == cod3) {
                            System.out.println("Código: " + p1.getCodProduto());
                            System.out.println("Nome: " + p1.getnProduto());
                            System.out.println("Quantidade em Estoque: " + p1.getQtdEstoque());
                            System.out.println("Custo Unitário: " + p1.getCustoUni());
                        }
                    }
                    break;

                case 5:
                    System.out.println("----------------------COMPRAS----------------------");

                    for (Produto p1 : listaProduto) {
                        for (Compra c : p1.getListaCompra()) {
                            System.out.printf("DATA    NOME      QUANTIDADE  VALOR_COMPRA%n");
                            System.out.printf("%-8s %-10s %-12d %-5f%n", c.getDataCompra(), c.getNomeCompra(), c.getQtdCompra(), + c.getValorCompra());
                        }
                    }

                    break;

                case 6:
                    System.out.println("----------------------VENDAS----------------------");

                    for (Produto p1 : listaProduto) {
                        for (Venda v : p1.getListaVenda()) {
                            System.out.println("DATA\tNOME\tQUANTIDADE\tVALOR VENDA\tCUSTO_UNITÁRIO");
                            System.out.println(v.getDataVenda() + "\t" + v.getNomeVenda() + "\t" + v.getQtdVenda() + "\t" + v.getValorVenda() + "\t" + v.getCustoUni());
                        }
                    }
                    break;
            }
        } while (opc != 0);

    }
}
