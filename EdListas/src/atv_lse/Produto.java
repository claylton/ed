package atv_lse;

import lse.ListaEncadeada;

public class Produto {

    private int codProduto, qtdEstoque;
    private String nProduto;
    private float custoUni;
    private ListaEncadeada<Compra> listaCompra = new ListaEncadeada<>();
    private ListaEncadeada<Venda> listaVenda = new ListaEncadeada<>();

    public Produto() {
        listaCompra = new ListaEncadeada<>();
        listaVenda = new ListaEncadeada<>();
    }

    public Produto(int codProduto,String nProduto) {
        this.codProduto = codProduto;
        this.qtdEstoque = 0;
        this.nProduto = nProduto;
        this.custoUni = 0;
    }

    public int getCodProduto() {
        return codProduto;
    }

    public void setCodProduto(int codProduto) {
        this.codProduto = codProduto;
    }

    public int getQtdEstoque() {
        return qtdEstoque;
    }

    public void setQtdEstoque(int qtdEstoque) {
        this.qtdEstoque = qtdEstoque;
    }

    public String getnProduto() {
        return nProduto;
    }

    public void setnProduto(String nProduto) {
        this.nProduto = nProduto;
    }

    public float getCustoUni() {
        return custoUni;
    }

    public void setCustoUni(float custoUni) {
        this.custoUni = custoUni;
    }

    public ListaEncadeada<Compra> getListaCompra() {
        return listaCompra;
    }

    public void setListaCompra(ListaEncadeada<Compra> listaCompra) {
        this.listaCompra = listaCompra;
    }

    public ListaEncadeada<Venda> getListaVenda() {
        return listaVenda;
    }

    public void setListaVenda(ListaEncadeada<Venda> listaVenda) {
        this.listaVenda = listaVenda;
    }
    
}
