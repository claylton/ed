package atv_lse;

public class Venda {
    private String dataVenda,nomeVenda;
    private int qtdVenda;
    private float valorVenda,custoUni;

    public Venda(String dataVenda, int qtdVenda, float valorVenda, float custoUni, String nomeVenda) {
        this.dataVenda = dataVenda;
        this.qtdVenda = qtdVenda;
        this.valorVenda = valorVenda;
        this.custoUni = custoUni;
        this.nomeVenda = nomeVenda;
    }

    public String getDataVenda() {
        return dataVenda;
    }

    public void setDataVenda(String dataVenda) {
        this.dataVenda = dataVenda;
    }

    public int getQtdVenda() {
        return qtdVenda;
    }

    public void setQtdVenda(int qtdVenda) {
        this.qtdVenda = qtdVenda;
    }

    public float getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(float valorVenda) {
        this.valorVenda = valorVenda;
    }

    public float getCustoUni() {
        return custoUni;
    }

    public void setCustoUni(float custoUni) {
        this.custoUni = custoUni;
    }

    public String getNomeVenda() {
        return nomeVenda;
    }

    public void setNomeVenda(String nomeVenda) {
        this.nomeVenda = nomeVenda;
    }
}
