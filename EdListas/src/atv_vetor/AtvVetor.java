package atv_vetor;

import java.util.Scanner;
import lista.vetor.ListaVetor;

public class AtvVetor {

    public static void main(String[] args) {
        ListaVetor<Disciplina> listaDisciplina = new ListaVetor();
        ListaVetor<Aluno> listaAluno = new ListaVetor();
        Scanner ler = new Scanner(System.in);
        int opc;

        do {
            System.out.println("------------------------MENU-----------------------");
            System.out.println("1 - Cadastrar disciplinas");
            System.out.println("2 - Matricular alunos em uma disciplina");
            System.out.println("3 - Listar alunos aprovados em uma disciplina");
            System.out.println("4 - Listar alunos reprovados em uma disciplina");
            System.out.println("5 - Listar alunos com a maior nota em uma disciplina");
            System.out.println("6 - Trancar aluno em uma disciplina");
            System.out.println("0 - Sair");
            System.out.println("----------------------------------------------------");
            System.out.print("Opção: ");
            opc = Integer.parseInt(ler.nextLine());
            System.out.println("----------------------------------------------------");

            switch (opc) {
                case 1:
                    System.out.print("Digite o nome da Disciplina: ");
                        String nDisc = ler.nextLine();
                            Disciplina d = new Disciplina(nDisc);
                                listaDisciplina.adicionar(d);
                    System.out.println("Disciplina "+d.getNome()+" adicionada !");            
                break;

                case 2:
                    System.out.println("Digite o nome da disciplina: ");
                    System.out.println("-----------------------------");
                        for (Disciplina ld : listaDisciplina) {
                            System.out.println(ld.getNome());
                        }
                    System.out.println("-----------------------------");
                    System.out.print("Nome: ");
                        String disc = ler.nextLine();
                    System.out.println("-----------------------------");
                        
                                for (Disciplina ld : listaDisciplina) {
                                    if (ld.getNome().equals(disc)) {
                                        System.out.println("Digite a matricula do Aluno: ");
                                            int mat = Integer.parseInt(ler.nextLine());
                                        System.out.println("Digite o nome do Aluno: ");
                                            String nAluno = ler.nextLine();
                                        System.out.println("Digite a nota1 do Aluno: ");
                                            int n1 = Integer.parseInt(ler.nextLine());
                                        System.out.println("Digite a nota2 do Aluno: ");
                                            int n2 = Integer.parseInt(ler.nextLine());
                                                Aluno a = new Aluno(mat, nAluno, n1, n2);
                                                    ld.getListaAluno().adicionar(a);
                                        System.out.println("Mattéria adicionada !");
                                        break;
                                    } else{
                                        System.out.println("Matéria não encontrada !");
                                        break;
                                    }
                                }
                break;

                case 3:
                    System.out.println("Digite o nome da disciplina: ");
                    System.out.println("-----------------------------");
                        for (Disciplina ld : listaDisciplina) {
                            System.out.println(ld.getNome());
                        }
                    System.out.println("-----------------------------");
                    System.out.print("Nome: ");
                        String disc1 = ler.nextLine();
                    System.out.println("-----------------------------");
                            for (Disciplina ld :listaDisciplina) {
                                if (ld.getNome().equals(disc1)) {
                                    for (Aluno la : ld.getListaAluno()) {
                                        if (la.media() >= 6) {
                                            System.out.println("Alunos aprovados na disiplina "+ld.getNome()+":");
                                            System.out.println("MATRÍCULA\tNOME\tNOTA 1\tNOTA 2\tMÉDIA");
                                            System.out.println(la.getMatricula()+"\t"+la.getNome()+"\t"+la.getNota1()+"\t"+la.getNota2()+"\t"+la.media());
                                        }
                                    }
                                break;
                                }
                    }
                break;
                    
                case 4:
                    System.out.println("Digite o nome da disciplina: ");
                    System.out.println("-----------------------------");
                        for (Disciplina ld : listaDisciplina) {
                            System.out.println(ld.getNome());
                        }
                    System.out.println("-----------------------------");
                    System.out.print("Nome: ");
                        String disc2 = ler.nextLine();
                    System.out.println("-----------------------------");
                            for (Disciplina ld : listaDisciplina) {
                                if (ld.getNome().equals(disc2)) {
                                    for (Aluno la : ld.getListaAluno()) {
                                        if (la.media() < 6) {
                                            System.out.println("Alunos reprovados na disiplina "+ld.getNome()+":");
                                            System.out.println("MATRÍCULA\tNOME\tNOTA 1\tNOTA 2\tMÉDIA");
                                            System.out.println(la.getMatricula()+"\t"+la.getNome()+"\t"+la.getNota1()+"\t"+la.getNota2()+"\t"+la.media());
                                        }
                                    }
                                }
                            }
                break;
                    
                case 5:
                    double notaMaior = 0;
                    int cont = 0;
                    System.out.println("Digite o nome da disciplina: ");
                    System.out.println("-----------------------------");
                        for (Disciplina ld : listaDisciplina) {
                            System.out.println(ld.getNome());
                        }
                    System.out.println("-----------------------------");
                    System.out.print("Nome: ");
                        String disc3 = ler.nextLine();
                    System.out.println("-----------------------------");
                            for (Disciplina ld : listaDisciplina) {
                                if (ld.getNome().equals(disc3)) {
                                    for (Aluno la : ld.getListaAluno()) {
                                        if (la.media() > notaMaior) {
                                            notaMaior = la.media();
                                        }
                                        if (notaMaior == la.media()) {
                                            System.out.println("Alunos destaque na disiplina "+ld.getNome()+":");
                                            System.out.println("MATRÍCULA\tNOME\tNOTA 1\tNOTA 2\tMÉDIA");
                                            System.out.println(la.getMatricula()+"\t"+la.getNome()+"\t"+la.getNota1()+"\t"+la.getNota2()+"\t"+la.media());
                                                cont++;
                                        }
                                    }
                                            if (cont != 0) {
                                                for (Aluno la : listaAluno) {
                                                    System.out.println("Alunos destaque na disiplina "+ld.getNome()+":");
                                                    System.out.println("MATRÍCULA\tNOME\tNOTA 1\tNOTA 2\tMÉDIA");
                                                    System.out.println(la.getMatricula()+"\t"+la.getNome()+"\t"+la.getNota1()+"\t"+la.getNota2()+"\t"+la.media());
                                        }
                                    }
                                }
                            }
                break;
                    
                case 6:
                    System.out.println("Digite o nome da disciplina: ");
                    System.out.println("-----------------------------");
                        for (Disciplina ld : listaDisciplina) {
                            System.out.println(ld.getNome());
                        }
                    System.out.println("-----------------------------");
                    System.out.print("Nome: ");
                        String disc4 = ler.nextLine();
                    System.out.println("-----------------------------");
                            for (Disciplina ld : listaDisciplina) {
                                if (ld.getNome().equals(disc4)) {
                                    System.out.println("Digite o nome do Aluno: ");
                                    System.out.println("-----------------------------");
                                        for (Aluno la : ld.getListaAluno()) {
                                            System.out.println(la.getNome());
                                        }
                                            System.out.println("-----------------------------");
                                                String alu = ler.nextLine();
                                                    for (Aluno la : ld.getListaAluno()) {
                                                        if (la.getNome().equals(alu)) {
                                                            ld.getListaAluno().removeItem(la);
                                                                System.out.println("Matrícula do aluno(a) "+la.getNome()+" trancada !");
                                                        }
                                                    }
                                }
                            }   
                break;
            }
        } while (opc != 0);
    }
}