package atv_vetor;

import lista.vetor.ListaVetor;

public class Disciplina {

    private String nome;
    private ListaVetor<Aluno> listaAluno = new ListaVetor<>();

    public Disciplina() {
        listaAluno = new ListaVetor<>();
    }

    public Disciplina(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ListaVetor<Aluno> getListaAluno() {
        return listaAluno;
    }
}
