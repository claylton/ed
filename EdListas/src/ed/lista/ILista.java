/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ed.lista;

/**
 *
 * @author ClayltonS
 */
public interface ILista<T> {
    
    public void adicionar(T elemento);
    public void adicionarNoInicio (T elemento);
    public void adicionarNoFim (T elemento);
    public void adicionar (T elemento, int posicao);
    
    public T remover (int posicao);
    public T removerNoInicio ();
    public T removerNoFim();
    public void removeItem(T elemento);
    
    public T buscar (int posicao);

            
    public int existe(T elemento);
    
    public int tamanho();
    
    public void limpar();
}
