package lde;

import ed.lista.ILista;
import java.util.Iterator;
import lse.Iterador;

public class ListaDuplamenteEncadeada<T> implements ILista<T>, Iterable<T> {

    private No<T> inicio;
    private No<T> fim;
    private int tamanho;

    public ListaDuplamenteEncadeada() {
        inicio = null;
        fim = null;
        tamanho = 0;
    }

    private class No<T> {

        T elemento;
        No<T> proximo;
        No<T> anterior;

        public No(T contem) {
            this.elemento = contem;
            this.proximo = null;
            this.anterior = null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterador();
    }

    public class Iterador implements Iterator<T> {

        private No<T> atual;

        public Iterador() {
            this.atual = inicio;
        }

        @Override
        public boolean hasNext() {
            return atual != null;
        }

        @Override
        public T next() {
            No<T> antigo = atual;
            atual = atual.proximo;
            return antigo.elemento;
        }

    }

    @Override
    public void adicionar(T elemento) {
        No<T> novo = new No<>(elemento);

        if (this.inicio == null) {
            this.inicio = fim = novo;
        } else {

            fim.proximo = novo;
            novo.anterior = fim;
            fim = novo;
        }
        this.tamanho++;
    }

    @Override
    public void adicionarNoInicio(T elemento) {
        No<T> novo = new No<>(elemento);

        if (tamanho == 0) {
            inicio = fim = novo;
        } else {
            inicio.anterior = novo;
            inicio.anterior.proximo = inicio;
            inicio = inicio.anterior;
        }
        this.tamanho++;
    }

    @Override
    public void adicionarNoFim(T elemento) {
        No<T> novo = new No<>(elemento);

        if (this.inicio == null) {
            this.inicio = fim = novo;
        } else {

            fim.proximo = novo;
            novo.anterior = fim;
            fim = novo;
        }
        this.tamanho++;
    }

    @Override
    public void adicionar(T elemento, int posicao) {
        No<T> novo = new No<>(elemento);
        double aux = this.tamanho / 2;

        if (posicao < 0 || posicao > tamanho - 1) {
            throw new IndexOutOfBoundsException();
        } else if (posicao == tamanho) {
            adicionarNoFim(elemento);
        } else if (posicao == 0) {
            adicionarNoInicio(elemento);
        } else if (aux >= posicao) {
            No<T> no = this.inicio;
            for (int i = 0; i < posicao - 1; i++) {
                no = no.proximo;
            }
            novo.proximo = no.proximo;
            no.proximo = novo;
            novo.proximo.anterior = novo;
            novo.anterior = no;
        } else {
            No<T> no = this.fim;
            for (int i = tamanho - 1; i > posicao; i++) {
                no = no.anterior;
            }
            novo.proximo = no;
            novo.anterior = no.anterior;
            novo.anterior.proximo = novo;
            no.anterior = novo;
        }
        this.tamanho++;
    }

    @Override
    public T remover(int posicao) {

        double aux = this.tamanho / 2;

        if (posicao < 0 || posicao > tamanho - 1) {
            throw new IndexOutOfBoundsException();
        } else if (posicao == tamanho - 1) {
            return removerNoFim();
        } else if (posicao == 0) {
            return removerNoInicio();
        } else if (aux <= posicao) {
            No<T> anterior = null;
            No<T> no = this.inicio;

            for (int i = 0; i < posicao; i++) {
                anterior = no;
                no = no.proximo;
            }
            No<T> antigo = anterior.proximo;
            anterior.proximo = no.proximo;
            no.proximo.anterior = anterior;
            this.tamanho--;
            return antigo.elemento;
        } else {
            No<T> anterior = null;
            No<T> no = this.fim;

            for (int i = 0; i < posicao; i++) {
                anterior = no;
                no = no.anterior;
            }
            No<T> antigo = anterior.proximo;
            anterior.proximo = no.proximo;
            no.proximo.anterior = anterior;
            this.tamanho--;
            return antigo.elemento;
        }
    }

    @Override
    public T removerNoInicio() {
        if (tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else if (tamanho == 1) {
            inicio = fim = null;
        } else {
            inicio = inicio.proximo;
            inicio.anterior = null;
        }
        tamanho--;
        return null;
    }

    @Override
    public T removerNoFim() {
        if (this.tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else if (this.tamanho == 1) {
            inicio = fim = null;
        } else {
            fim = fim.anterior;

            fim.proximo = null;
        }
        this.tamanho--;
        return null;
    }

    @Override
    public void removeItem(T elemento) {
        if (this.tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else {
            for (int i = 0; i < this.tamanho; i++) {
                if (buscar(i).equals(elemento)) {
                    remover(i);
                }
            }
        }
    }

    @Override
    public T buscar(int posicao) {
        if (posicao >= 0 && posicao < tamanho) {
            No<T> no = inicio;
            for (int i = 0; i < posicao; i++) {
                no = no.proximo;
            }
            return no.elemento;
        }
        return null;
    }

    @Override
    public int existe(T elemento) {
        No<T> no = this.inicio;
        int i = 0;
        while (no.proximo != null) {
            if (no.elemento == elemento) {
                return i;
            }
            i++;
            no = no.proximo;
        }
        return -1;
    }

    @Override
    public int tamanho() {
        return tamanho;
    }

    @Override
    public void limpar() {
        while (this.tamanho > 1) {
            removerNoInicio();
        }
        this.inicio = this.fim = null;
        this.tamanho = 0;
    }

}
