package lde;

public class Teste {

    public static void main(String[] args) {
        ListaDuplamenteEncadeada<Integer> ld = new ListaDuplamenteEncadeada<>();
        
        ld.adicionar(1);
        ld.adicionar(2);
        ld.adicionar(3);
        ld.adicionarNoInicio(0);
        ld.adicionar(100, 2);
        ld.removerNoInicio();
        ld.removerNoFim();
        ld.remover(1);
        ld.removeItem(2);
//        ld.limpar();
        
        for (Integer l : ld) {
            System.out.println(l);
        }
        
        System.out.println("Tamanho: "+ld.tamanho());
    }
}
