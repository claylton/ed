package lista.vetor;

import ed.lista.ILista;
import java.util.Iterator;

public class ListaVetor<T> implements ILista<T>, Iterable<T> {

    int posicao;
    private T[] vetor;
    private int tamanho;//Quantidade de elementos na lista
    private int capacidade; //Tamanho do vetor

    public ListaVetor() {
        this.vetor = (T[]) new Object[10];
    }

    public void aumentaTamanho() {
        if (tamanho == capacidade) {
            int capacidadeAntiga = capacidade;
            capacidade *= 2;
            T[] novoVetor = (T[]) new Object[capacidade];
            for (int i = 0; i < capacidadeAntiga; i++) {
                novoVetor[i] = vetor[i];
            }
            this.vetor = novoVetor;
        }
    }

    @Override
    public void adicionar(T elemento) {
        if (vetor.length != capacidade) {
            vetor[tamanho] = elemento;
            tamanho++;
        } else {
            aumentaTamanho();
            vetor[tamanho] = elemento;
            tamanho++;
        }
    }

    @Override
    public void adicionarNoInicio(T elemento) {
        if (vetor.length != capacidade) {
            for (int i = tamanho; i > 0; i--) {
                vetor[i] = vetor[i - 1];
            }
            vetor[0] = elemento;
            tamanho++;
        } else {
            aumentaTamanho();
            for (int i = tamanho; i > 0; i--) {
                vetor[i] = vetor[i - 1];
            }
            vetor[0] = elemento;
            tamanho++;
        }
    }

    @Override
    public void adicionarNoFim(T elemento) {
        if (vetor.length != capacidade) {
            vetor[tamanho] = elemento;
            tamanho++;
        } else {
            aumentaTamanho();
            vetor[tamanho] = elemento;
            tamanho++;
        }
    }

    @Override
    public void adicionar(T elemento, int posicao) {
        if (posicao < 0) {
            throw new IndexOutOfBoundsException();
        } else if (posicao == tamanho) {
            adicionarNoFim(elemento);
        } else if (posicao == 0) {
            adicionarNoInicio(elemento);
        } else {
            if (vetor.length != tamanho) {
                for (int i = tamanho; i > 0; i--) {
                    vetor[i] = vetor[i - 1];
                }
                vetor[posicao] = elemento;
                tamanho++;
            } else {
                aumentaTamanho();
                for (int i = tamanho; i > 0; i--) {
                    vetor[i] = vetor[i - 1];
                }
                vetor[posicao] = elemento;
                tamanho++;
            }
        }
    }

    @Override
    public int existe(T elemento) {
        for (int i = 0; i < tamanho; i++) {
            if (elemento.equals(buscar(i))) {
                return i;
            }
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public void limpar() {
        for (int i = tamanho; i > 0; i--) {
            remover(i);
        }
    }

    @Override
    public T remover(int posicao) {
        if (tamanho == 0 || posicao < 0) {
            throw new IndexOutOfBoundsException();
        } else if (posicao == tamanho) {
            removerNoFim();
        } else if (posicao == 0) {
            removerNoInicio();
        } else {
            for (int i = posicao; i < tamanho; i++) {
                vetor[i] = vetor[i + 1];
            }
            tamanho--;
            return vetor[tamanho];
        }
        return null;
    }

    @Override
    public T removerNoInicio() {
        if (tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else {
            for (int i = 0; i < tamanho; i++) {
                vetor[i] = vetor[i + 1];
            }
            vetor[tamanho] = null;
            tamanho--;
            return vetor[tamanho];
        }
    }

    @Override
    public T removerNoFim() {
        if (tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else {
            vetor[tamanho - 1] = null;
            tamanho--;
            return vetor[tamanho];
        }
    }

    @Override
    public void removeItem(T elemento) {
        if (tamanho == 0) {
            throw new IndexOutOfBoundsException();
        }
        for (int i = 0; i < tamanho; i++) {
            if (elemento.equals(vetor[i])) {
                remover(i);
                break;
            }
        }
    }

    @Override
    public int tamanho() {
        return tamanho;
    }

    @Override
    public T buscar(int posicao) {
        if (posicao >= 0 && posicao < tamanho) {
            return vetor[posicao];
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteradorVetor();
    }

    private class IteradorVetor implements Iterator<T> {

        private int i = - 1;

        @Override
        public boolean hasNext() {
            if (i < tamanho() - 1) {
                i++;
                return true;
            }
            return false;
        }

        @Override
        public T next() {
            return (T) buscar(i);
        }

    }

}