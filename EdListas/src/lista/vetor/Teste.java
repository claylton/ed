package lista.vetor;

public class Teste {

    public static void main(String[] args) {
        ListaVetor<Integer> v = new ListaVetor();

        v.adicionar(100);
        v.adicionarNoFim(200);
        v.adicionarNoInicio(000);
//        v.removerNoInicio();
//        v.removerNoFim();
          v.removeItem(100);
//        v.remover(0);
//         v.limpar();
        
        for (Integer v1 : v) {
            System.out.print(v1+", ");
        }
        System.out.println("\nTamanho: "+v.tamanho());
        System.out.println();
 //       System.out.println(v.buscar(1));
 //       System.out.println(v.existe(200));
        System.out.println(v.buscar(0));
        System.out.println(v.buscar(1));
    }

}
