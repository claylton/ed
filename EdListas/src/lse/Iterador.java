package lse;

import java.util.Iterator;


public class Iterador<T> implements Iterator<T> {
    
    private No<T> no;

    public Iterador(No<T> no) {
        this.no = no;
    }
    
    @Override
    public boolean hasNext() {
        return no != null;
    }

    @Override
    public T next() {
        T valor = (T) no.getElemento();
        no = no.getProximo();
        return valor;
    }
}