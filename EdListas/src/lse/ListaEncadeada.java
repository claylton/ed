package lse;

import ed.lista.ILista;
import java.util.Iterator;

public class ListaEncadeada<T> implements ILista<T>, Iterable<T> {

    private No<T> inicio, fim;
    private int tamanho;

    public ListaEncadeada() {
        this.inicio = null;
        this.fim = null;
        this.tamanho = 0;
    }

    @Override
    public void adicionar(T elemento) {
        No<T> no = new No<>();

        no.setElemento(elemento);

        if (this.tamanho == 0) {
            this.inicio = this.fim = no;
        } else {
            this.fim.setProximo(no);
            this.fim = no;
            no.setProximo(null);
        }
        this.tamanho++;
    }

    @Override
    public void adicionarNoInicio(T elemento) {
        No<T> no = new No<>();

        no.setElemento(elemento);

        if (this.tamanho == 0) {
            this.inicio = this.fim = no;
        } else {
            no.setProximo(inicio);
            this.inicio = no;
        }
        this.tamanho++;
    }

    @Override
    public void adicionarNoFim(T elemento) {
        No<T> no = new No<>();

        no.setElemento(elemento);

        if (this.tamanho == 0) {
            this.inicio = this.fim = no;
        } else {
            this.fim.setProximo(no);
            this.fim = no;
        }
        this.tamanho++;
    }

    @Override
    public void adicionar(T elemento, int posicao) {
        No<T> no, aux, novoNo = new No<>();
        novoNo.setElemento(elemento);

        no = this.inicio;
        for (int i = 0; i < posicao - 1; i++) {
            no = no.getProximo();
        }

        aux = no.getProximo();
        no.setProximo(novoNo);
        novoNo.setProximo(aux);

        this.tamanho++;
    }

    @Override
    public T remover(int posicao) {
        No<T> no, aux = new No<>();

        if (this.tamanho == 0 || posicao < 0) {
            throw new IndexOutOfBoundsException();
        } else if (posicao == this.tamanho - 1) {
            removerNoFim();
        } else if (posicao == 0) {
            removerNoInicio();
        } else {
            no = this.inicio;
            for (int i = 0; i < posicao; i++) {
                aux = no;
                no = no.getProximo();
            }
            aux.setProximo(no.getProximo());
            this.tamanho--;
        }
        return null;
    }

    @Override
    public T removerNoInicio() {
        if (this.tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else {
            this.inicio = inicio.getProximo();
            this.tamanho--;
        }
        return null;
    }

    @Override
    public T removerNoFim() {
        No<T> no = new No<>();

        if (this.tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else {
            no = this.inicio;
            for (int i = 0; i < tamanho - 2; i++) {
                no = no.getProximo();
            }
            no.setProximo(null);
            this.fim = no;
            this.tamanho--;
        }
        return null;
    }

    @Override
    public void removeItem(T elemento) {
        if (this.tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else {
            for (int i = 0; i < this.tamanho; i++) {
                if (buscar(i) == elemento) {
                    remover(i);
                }
            }
        }

    }

    @Override
    public T buscar(int posicao) {
        No<T> no = new No<>();

        if (this.tamanho == 0) {
            throw new IndexOutOfBoundsException();
        } else if (posicao < 0 || posicao > this.tamanho - 1) {
            throw new IndexOutOfBoundsException();
        } else if (posicao == 0) {
            return this.inicio.getElemento();
        } else if (posicao == this.tamanho) {
            return this.fim.getElemento();
        } else {
            no = this.inicio;
            for (int i = 0; i < posicao; i++) {
                no = no.getProximo();
            }
        }

        return no.getElemento();
    }

    @Override
    public int existe(T elemento) {
        for (int i = 0; i < this.tamanho; i++) {
            if (elemento.equals(buscar(i))) {
                return i;
            }
        }
        throw new IndexOutOfBoundsException();
    }

    @Override
    public int tamanho() {
        return tamanho;
    }

    @Override
    public void limpar() {
        while (this.tamanho > 1) {
            removerNoInicio();
        }
        this.inicio = this.fim = null;
        this.tamanho = 0;
    }

    @Override
    public Iterator<T> iterator() {
        lse.Iterador<T> it = new lse.Iterador<>(inicio);
        return it;
    }
}
