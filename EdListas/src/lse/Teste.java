package lse;

public class Teste {

    public static void main(String[] args) {
        ListaEncadeada<Integer> l = new ListaEncadeada<>();

        l.adicionar(1);
        l.adicionar(2);
        l.adicionar(3);
        l.removerNoFim();
//        l.adicionarNoInicio(0);
//        l.adicionarNoFim(4);
//        l.adicionar(6100, 3);
//        l.remover(3);
//        l.removerNoInicio();
//        l.removerNoFim();
//        l.removeItem(2);
//        l.limpar();
 
        for (Integer l1 : l) {
            System.out.println(l1);
        }
        System.out.println("Tamanho: "+l.tamanho());
        System.out.println("Elemento: "+l.existe(1));
    }
}
